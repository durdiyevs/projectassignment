import React, { Fragment, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { Form } from "react-bootstrap";
import { login } from "../../actions/index";
import { connect } from "react-redux";

const Login = ({ login, authReducers: { isAuthenticated } }) => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });
  const { email, password } = formData;
  const onHandleChange = (e) =>
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  const onSubmit = (e) => {
    e.preventDefault();
    const sendDataLogin = {
      email,
      password,
    };
    login(sendDataLogin);
  };
  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }
  return (
    <Fragment>
      <h1 className="text-primary text-center">Sign In</h1>
      <h4 className="text-dark mb-5 text-center">Sign into Your Account</h4>
      <div className="mx-auto w-25">
        <Form onSubmit={onSubmit}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label className="text-gray">
              Enter your email address
            </Form.Label>
            <Form.Control
              type="email"
              value={email}
              name="email"
              onChange={onHandleChange}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label className="text-gray">Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              value={password}
              onChange={onHandleChange}
              required
            />
          </Form.Group>

          <div className="w-100 text-center">
            <input type="submit" className="btn btn-primary" value="Login" />
          </div>
        </Form>
      </div>
      <p className="mt-2 text-dark text-center">
        Don't have an account?{" "}
        <Link to="/register" className="text-primary">
          Sign Up
        </Link>
      </p>
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    authReducers: state.authReducers,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    login: (sendDataLogin) => {
      dispatch(login(sendDataLogin));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
