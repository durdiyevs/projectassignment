import React, { Fragment, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { Form } from "react-bootstrap";
import { connect } from "react-redux";
import { setAlert, register } from "../../actions/index";

const Register = ({
  setAlert,
  register,
  authReducers: { isAuthenticated },
}) => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
    password2: "",
  });
  const { name, email, password, password2 } = formData;
  const onHandleChange = (e) =>
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  const onSubmit = async (e) => {
    e.preventDefault();
    if (password !== password2) {
      setAlert("Password do not math", "danger");
    } else {
      const sendDataRegister = {
        name,
        email,
        password,
      };
      register(sendDataRegister);
    }
  };
  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Fragment>
      <h1 className="text-primary text-center">Sign Up</h1>
      <h4 className="text-dark mb-5 text-center">Create Your Account</h4>
      <div className="mx-auto w-25">
        <Form onSubmit={onSubmit}>
          <Form.Group className="mb-3 text-left">
            <Form.Label className="text-gray">Enter your name</Form.Label>
            <Form.Control
              type="text"
              name="name"
              value={name}
              onChange={onHandleChange}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label className="text-gray">
              Enter your email address
            </Form.Label>
            <Form.Control
              type="email"
              value={email}
              name="email"
              onChange={onHandleChange}
              required
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label className="text-gray">Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              value={password}
              onChange={onHandleChange}
              minLength="6"
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label className="text-gray">Password confirmation</Form.Label>
            <Form.Control
              type="password"
              name="password2"
              value={password2}
              onChange={onHandleChange}
              minLength="6"
              required
            />
          </Form.Group>

          <div className="w-100 text-center">
            <input type="submit" className="btn btn-primary" value="Register" />
          </div>
        </Form>
        <p className="mt-2 text-dark text-center">
          Already have an account?{" "}
          <Link to="/login" className="text-primary">
            Sign In
          </Link>
        </p>
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    authReducers: state.authReducers,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    setAlert: (msg, alertType) => {
      dispatch(setAlert(msg, alertType));
    },
    register: (sendDataRegister) => {
      dispatch(register(sendDataRegister));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
