import React, { useEffect, Fragment } from "react";
import { connect } from "react-redux";
import { getCurrentProfile } from "../../actions/index";
import { Link } from "react-router-dom";
import DashboardActions from "./DashboardActions";
import Experience from "./Experience";
import Education from "./Education";
import { deleteAccount } from "../../actions/index";

const Dashboard = ({
  getCurrentProfile,
  authReducers: { user },
  profileReducers: { profile },
  deleteAccount,
}) => {
  useEffect(() => {
    getCurrentProfile();
  }, [getCurrentProfile]);
  return (
    <Fragment>
      <h1 className="text-primary text-center">Dashboard</h1>
      <h4 className="text-gray mb-5 text-center">
        Welcome {user && user.name}
      </h4>
      {profile !== null ? (
        <Fragment>
          <DashboardActions />
          <Experience experience={profile.experience} />
          <div className="mt-5">
            <Education education={profile.education} />
          </div>
          <div className="w-100 text-center my-5">
            <button className="btn btn-danger" onClick={() => deleteAccount()}>
              Delete my account
            </button>
          </div>
        </Fragment>
      ) : (
        <Fragment>
          <p className="text-center text-dark">
            You have not yet setup a profile, please add some info
          </p>
          <div className="w-100 text-center">
            <Link to="/create-profile" className="btn btn-primary my-1">
              Create Profile
            </Link>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    profileReducers: state.profileReducers,
    authReducers: state.authReducers,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    getCurrentProfile: () => {
      dispatch(getCurrentProfile());
    },
    deleteAccount: () => {
      dispatch(deleteAccount());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
