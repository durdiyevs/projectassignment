import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const DashboardActions = () => {
  return (
    <Fragment>
      <div className="text-center mb-5">
        <Link to="/edit-profile" className="btn btn-primary">
          <i className="fas fa-user-circle text-light" /> Edit Profile
        </Link>
        <Link to="/add-experience" className="btn btn-primary mx-2">
          <i className="fab fa-black-tie text-light" /> Add Experience
        </Link>
        <Link to="/add-education" className="btn btn-primary">
          <i className="fas fa-graduation-cap text-light" /> Add Education
        </Link>
      </div>
    </Fragment>
  );
};
export default DashboardActions;
