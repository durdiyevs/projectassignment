import React, { Fragment } from "react";
import Moment from "react-moment";
import moment from "moment";
import { connect } from "react-redux";
import { deleteEducation } from "../../actions/index";

const Education = ({ education, deleteEducation }) => {
  const educations = education.map((edu) => (
    <tr key={edu._id}>
      <td className="w-35">{edu.school}</td>
      <td className="w-35 hide-sm">{edu.degree}</td>
      <td className="w-25">
        <Moment format="YYYY/MM/DD">{moment.utc(edu.from)}</Moment> -{" "}
        {edu.to === null ? (
          " Now"
        ) : (
          <Moment format="YYYY/MM/DD">{moment.utc(edu.to)}</Moment>
        )}
      </td>
      <td className="w-5">
        <button
          onClick={() => deleteEducation(edu._id)}
          className="btn btn-danger"
        >
          <i className="fas fa-trash text-light" />
        </button>
      </td>
    </tr>
  ));

  return (
    <Fragment>
      <h4 className="my-2 text-dark text-uppercase text-center">Education</h4>
      <table className="table">
        <thead>
          <tr>
            <th className="w-35">School</th>
            <th className="w-35 hide-sm">Degree</th>
            <th className="w-25 hide-sm">Years</th>
            <th className="w-5"/>
          </tr>
        </thead>
        <tbody>{educations}</tbody>
      </table>
    </Fragment>
  );
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    deleteEducation: (id) => {
      dispatch(deleteEducation(id));
    },
  };
};

export default connect(null, mapDispatchToProps)(Education);
