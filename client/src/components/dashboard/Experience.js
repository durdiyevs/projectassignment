import React, { Fragment } from "react";
import { connect } from "react-redux";
import Moment from "react-moment";
import moment from "moment";
import { deleteExperience } from "../../actions/index";

const Experience = ({ experience, deleteExperience }) => {
  const experiences = experience.map((exp) => (
    <tr key={exp._id}>
      <td>{exp.company}</td>
      <td className="hide-sm">{exp.title}</td>
      <td>
        <Moment format="YYYY/MM/DD">{moment.utc(exp.from)}</Moment> -{" "}
        {exp.to === null ? (
          " Now"
        ) : (
          <Moment format="YYYY/MM/DD">{moment.utc(exp.to)}</Moment>
        )}
      </td>
      <td className="w-5">
        <button
          onClick={() => deleteExperience(exp._id)}
          className="btn btn-danger"
        >
          <i className="fas fa-trash text-light"/>
        </button>
      </td>
    </tr>
  ));
  return (
    <Fragment>
      <h4 className="my-2 text-dark text-uppercase text-center">Experience</h4>
      <table className="table">
        <thead>
          <tr>
            <th className="w-35">Company</th>
            <th className="w-35 hide-sm">Title</th>
            <th className="w-25 hide-sm">Years</th>
            <th className="w-5"/>
          </tr>
        </thead>
        <tbody>{experiences}</tbody>
      </table>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    deleteExperience: (id) => {
      dispatch(deleteExperience(id));
    },
  };
};
export default connect(null, mapDispatchToProps)(Experience);
