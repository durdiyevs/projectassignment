import React, { Fragment } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { connect } from "react-redux";
import { logout } from "../../actions/index";

const CustomNavbar = ({
  authReducers: { isAuthenticated, loading },
  logout,
}) => {
  const authLinks = (
    <Nav className="ms-auto">
      <Nav.Link className="me-3 text-20" href="/profiles">
        Users
      </Nav.Link>
      <Nav.Link className="mx-3 text-20" href="/posts">
        Posts
      </Nav.Link>
      <Nav.Link className="mx-3 text-20" href="/dashboard">
        <i className="fas fa-user" /> Dashboard
      </Nav.Link>
      <Nav.Link className="ms-3 text-20" onClick={logout} href="#!">
        <i className="fas fa-sign-out-alt" /> Sign out
      </Nav.Link>
    </Nav>
  );

  const guestLinks = (
    <Nav className="ms-auto">
      <Nav.Link className="me-3 text-20" href="/profiles">
        Users
      </Nav.Link>
      <Nav.Link className="mx-3 text-20" href="/register">
        <i className="fas fa-user" /> Sign up
      </Nav.Link>
      <Nav.Link className="ms-3 text-20" href="/login">
        <i className="fas fa-sign-in-alt" /> Sign in
      </Nav.Link>
    </Nav>
  );

  return (
    <Navbar bg="transparent" variant="dark" className="py-4">
      <Container>
        <Navbar.Brand href="/" className="d-flex align-items-center">
          <h4 className="ms-3 mb-0">Project Assignment</h4>
        </Navbar.Brand>

        {!loading && (
          <Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>
        )}
      </Container>
    </Navbar>
  );
};

const mapStateToProps = (state) => {
  return {
    authReducers: state.authReducers,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    logout: () => {
      dispatch(logout());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomNavbar);
