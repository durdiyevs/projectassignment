import React from "react";
import { Button } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

const Landing = ({ isAuthenticated }) => {
  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <section>
      <div className="dark-overlay">
        <div className="landing-inner">
          <h1 className="x-large text-dark">
            React.js <span className="text-primary">Project Assignment</span>
            <br /> with Javascript
          </h1>
          <p className="text-gray">
            My task is to design and implement a web application using React.js.
            Use a service like Kinvey or Firebase for your back-end or create
            your own with Node.js and MongoDB or a framework in another language
            (ASP.NET, Spring, Symfony). It can be a discussion forum, blog
            system, e-commerce site, online gaming site, social network, or any
            other web application by your choice.
          </p>
          <div>
            <Button className="me-2" variant="primary" href="/register">
              Sign Up
            </Button>
            <Button className="ms-2" variant="primary" href="/login">
              Login
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.authReducers.isAuthenticated,
});

export default connect(mapStateToProps, null)(Landing);
