import React, { useState } from "react";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";
import { addComment } from "../../actions/index";

const CommentForm = ({ postId, addComment }) => {
  const [text, setText] = useState("");

  return (
    <div className="mx-auto w-50">
      <h4 className="text-gray text-center mb-5">Leave a Comment</h4>
      <Form
        className="form my-1"
        onSubmit={(e) => {
          e.preventDefault();
          addComment(postId, { text });
          setText("");
        }}
      >
        <Form.Group className="mb-3" controlId="formBasicSkills">
          <Form.Label className="text-gray">Comment the post</Form.Label>
          <Form.Text
            className="d-block w-100"
            as="textarea"
            rows={5}
            name="text"
            value={text}
            onChange={(e) => setText(e.target.value)}
            required
          />
        </Form.Group>

        <input type="submit" className="btn btn-primary my-1" value="Submit" />
      </Form>
    </div>
  );
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    addComment: (postId, formData) => {
      dispatch(addComment(postId, formData));
    },
  };
};
export default connect(null, mapDispatchToProps)(CommentForm);
