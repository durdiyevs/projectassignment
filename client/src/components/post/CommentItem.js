import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Card } from "react-bootstrap";
import Moment from "react-moment";
import { deleteComment } from "../../actions/index";

const CommentItem = ({
  postId,
  comment: { _id, text, name, avatar, user, date },
  authReducers,
  deleteComment,
}) => (
  <div className="mx-auto w-75">
    <Card className="d-flex border-0 flex-row px-5 py-3 my-3">
      <div>
        <Link to={`/profile/${user}`}>
          <div className="w-100 text-center">
            <img
              className="round-img ms-2"
              src={avatar}
              alt=""
              style={{ height: "84px", width: "84px" }}
            />
          </div>
          <h4 className="text-center mt-3 mb-0">{name}</h4>
        </Link>
        <p className="text-16 text-gray text-center">
          Posted on <Moment format="YYYY/MM/DD">{date}</Moment>
        </p>
      </div>
      <div className="ps-4">
        <p className="my-1 text-gray">{text}</p>
        {!authReducers.loading && user === authReducers.user._id && (
          <button
            onClick={() => deleteComment(postId, _id)}
            type="button"
            className="btn btn-danger"
          >
            <i className="fas fa-trash" />
          </button>
        )}
      </div>
    </Card>
  </div>
);

const mapStateToProps = (state) => {
  return {
    authReducers: state.authReducers,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    deleteComment: (postId, commentId) => {
      dispatch(deleteComment(postId, commentId));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentItem);
