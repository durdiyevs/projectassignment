import React, { useState } from "react";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";
import { addPost } from "../../actions/index";

const PostForm = ({ addPost }) => {
  const [text, setText] = useState("");

  return (
    <div className="post-form">
      <div className="mx-auto w-50">
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            addPost({ text });
            setText("");
          }}
        >
          <Form.Group className="mb-3" controlId="formBasicSkills">
            <Form.Label className="text-gray">Create a post</Form.Label>
            <Form.Text
              className="d-block w-100"
              as="textarea"
              rows={10}
              type="text"
              name="text"
              value={text}
              onChange={(e) => setText(e.target.value)}
              required
            />
            <Form.Text className="text-muted">
              Tell us something new about the development and engineering ...
            </Form.Text>
          </Form.Group>
          <input
            type="submit"
            className="btn btn-primary my-1"
            value="Submit"
          />
        </Form>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    addPost: (formData) => {
      dispatch(addPost(formData));
    },
  };
};

export default connect(null, mapDispatchToProps)(PostForm);
