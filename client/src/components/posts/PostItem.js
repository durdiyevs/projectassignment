import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import Moment from "react-moment";
import { connect } from "react-redux";
import { addLike, removeLike, deletePost } from "../../actions/index";

const PostItem = ({
  addLike,
  removeLike,
  deletePost,
  authReducers,
  post: { _id, text, name, avatar, user, likes, comments, date },
  showActions,
}) => (
  <div className="mx-auto w-75">
    <Card className="d-flex border-0 flex-row px-5 py-4 my-3">
      <div>
        <Link to={`/profile/${user}`}>
          <img className="round-img ms-2" src={avatar} alt="" />
          <h4 className="text-center mt-3 mb-0">{name}</h4>
        </Link>
        <p className="text-16 text-gray text-center">
          Posted on <Moment format="YYYY/MM/DD">{date}</Moment>
        </p>

        {showActions && (
          <Fragment>
            <div className="text-center w-100">
              <button
                onClick={() => addLike(_id)}
                type="button"
                className="btn btn-light"
              >
                <i className="fas fa-thumbs-up" />{" "}
                <span>{likes.length > 0 && <span>{likes.length}</span>}</span>
              </button>
              <button
                onClick={() => removeLike(_id)}
                type="button"
                className="btn btn-light ms-2"
              >
                <i className="fas fa-thumbs-down" />
              </button>
            </div>
          </Fragment>
        )}
      </div>

      <div>
        <p className="mb-3 text-dark">{text}</p>
        {showActions && (
          <Fragment>
            <Link to={`/posts/${_id}`} className="btn btn-primary">
              <i className="fas fa-comment" />
              {comments.length > 0 && (
                <span className="comment-count ms-2">{comments.length}</span>
              )}
            </Link>
            {!authReducers.loading && user === authReducers.user._id && (
              <button
                onClick={() => deletePost(_id)}
                type="button"
                className="btn btn-danger ms-2"
              >
                <i className="fas fa-trash" />
              </button>
            )}
          </Fragment>
        )}
      </div>
    </Card>
  </div>
);

PostItem.defaultProps = {
  showActions: true,
};
const mapStateToProps = (state) => {
  return {
    authReducers: state.authReducers,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    addLike: (id) => {
      dispatch(addLike(id));
    },
    removeLike: (id) => {
      dispatch(removeLike(id));
    },
    deletePost: (id) => {
      dispatch(deletePost(id));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(PostItem);
