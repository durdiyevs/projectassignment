import React, { Fragment, useEffect } from "react";
import { connect } from "react-redux";
import { getPosts } from "../../actions/index";
import PostItem from "./PostItem";
import PostForm from "./PostForm";
const Posts = ({ getPosts, postReducers: { posts } }) => {
  useEffect(() => {
    getPosts();
  }, [getPosts]);

  return (
    <Fragment>
      <h1 className="text-primary text-center">Posts</h1>
      <h4 className="text-gray text-center mb-5">Tell us something new</h4>
      <PostForm />
      <div className="posts">
        {posts.map((post) => (
          <PostItem key={post._id} post={post} />
        ))}
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    postReducers: state.postReducers,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getPosts: () => {
      dispatch(getPosts());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
