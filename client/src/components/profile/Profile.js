import React, { useEffect, Fragment } from "react";
import { connect } from "react-redux";
import { Row, Col } from "react-bootstrap";
import { getProfileById } from "../../actions";
import Spinner from "../layout/Spinner";
import { Link } from "react-router-dom";
import ProfileTop from "./ProfileTop";
import ProfileAbout from "./ProfileAbout";
import ProfileExperience from "./ProfileExperience";
import ProfileEducation from "./ProfileEducation";
import ProfileGithub from "./ProfileGithub";

const Profile = ({
  getProfileById,
  profileReducers: { profile, loading },
  authReducers,
  match,
}) => {
  useEffect(() => {
    getProfileById(match.params.id);
  }, [getProfileById]);
  return (
    <Fragment>
      {profile === null || loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <Link to="/profiles" className="btn btn-danger">
            <i className="fas fa-arrow-left me-2"></i>
            Back
          </Link>
          {authReducers.isAuthenticated &&
            authReducers.loading === false &&
            authReducers.user._id === profile.user._id && (
              <Link to="/edit-profile" className="btn btn-primary ms-2">
                <i className="fas fa-edit me-2"></i>
                Edit
              </Link>
            )}
          <div className="mt-5">
            <Row>
              <Col xs={12} md={5} lg={4}>
                <ProfileTop profile={profile} />
              </Col>

              <Col xs={12} md={7} lg={8}>
                <ProfileAbout profile={profile} />

                <Row>
                  <Col xs={12} md={6} lg={6}>
                    <div className="p-2">
                      <h4 className="text-dark">Experience</h4>
                      {profile.experience.length > 0 ? (
                        <Fragment>
                          {profile.experience.map((experience) => (
                            <ProfileExperience
                              key={experience._id}
                              experience={experience}
                            />
                          ))}
                        </Fragment>
                      ) : (
                        <h4 className="text-gray">No experience credentials</h4>
                      )}
                    </div>
                  </Col>
                  <Col xs={12} md={6} lg={6}>
                    <div className="p-2">
                      <h4 className="text-dark">Education</h4>
                      {profile.education.length > 0 ? (
                        <Fragment>
                          {profile.education.map((education) => (
                            <ProfileEducation
                              key={education._id}
                              education={education}
                            />
                          ))}
                        </Fragment>
                      ) : (
                        <h4 className="text-gray">No education credentials</h4>
                      )}
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>

            {profile.githubusername && (
              <ProfileGithub username={profile.githubusername} />
            )}
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    profileReducers: state.profileReducers,
    authReducers: state.authReducers,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    getProfileById: (userId) => {
      dispatch(getProfileById(userId));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
