import React, { Fragment } from "react";

const ProfileAbout = ({
  profile: {
    bio,
    skills,
    user: { name },
  },
}) => (
  <div className="profile-about p-2">
    {bio && (
      <Fragment>
        <h2 className="text-dark">{name.trim().split(" ")[0]}s Bio</h2>
        <p className="text-gray">{bio}</p>
        <div className="line" />
      </Fragment>
    )}
    <h4 className="text-dark">Skill Set</h4>
    <div className="skills">
      {skills.map((skill, index) => (
        <div key={index} className="p-1 text-gray">
          <i className="fas fa-check" /> {skill}
        </div>
      ))}
    </div>
    <div className="line" />
  </div>
);

export default ProfileAbout;
