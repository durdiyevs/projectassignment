import React from "react";
import Moment from "react-moment";
import moment from "moment";

const ProfileEducation = ({
  education: { school, degree, fieldofstudy, current, to, from, description },
}) => (
  <div>
    <h4 className="text-dark">{school}</h4>
    <p className="text-gray">
      <Moment format="YYYY/MM/DD">{moment.utc(from)}</Moment> -{" "}
      {!to ? " Now" : <Moment format="YYYY/MM/DD">{moment.utc(to)}</Moment>}
    </p>
    <p className="text-gray">
      <strong>Degree: </strong> {degree}
    </p>
    <p className="text-gray">
      <strong>Field Of Study: </strong> {fieldofstudy}
    </p>
    <p className="text-gray">
      <strong>Description: </strong> {description}
    </p>
  </div>
);

export default ProfileEducation;
