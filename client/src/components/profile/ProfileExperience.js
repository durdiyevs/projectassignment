import React from "react";
import Moment from "react-moment";
import moment from "moment";

const ProfileExperience = ({
  experience: { company, title, location, current, to, from, description },
}) => (
  <div>
    <h4 className="text-dark">{company}</h4>
    <p className="text-gray">
      <Moment format="YYYY/MM/DD">{moment.utc(from)}</Moment> -{" "}
      {!to ? " Now" : <Moment format="YYYY/MM/DD">{moment.utc(to)}</Moment>}
    </p>
    <p className="text-gray">
      <strong>Position: </strong> {title}
    </p>
    <p className="text-gray">
      <strong>Location: </strong> {location}
    </p>
    <p className="text-gray">
      <strong>Description: </strong> {description}
    </p>
  </div>
);

export default ProfileExperience;
