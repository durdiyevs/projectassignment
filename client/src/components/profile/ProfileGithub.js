import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Card, Row, Col } from "react-bootstrap";
import Spinner from "../layout/Spinner";
import { getGithubRepos } from "../../actions/index";

const ProfileGithub = ({ username, getGithubRepos, repos }) => {
  useEffect(() => {
    getGithubRepos(username);
  }, [getGithubRepos, username]);

  return (
    <div className="profile-github">
      <h4 className="text-dark mt-5">Github Repositories</h4>
      {repos === null ? (
        <Spinner />
      ) : (
        repos.map((repo) => (
          <Col xs={12} md={12} lg={6}>
            <Card
              key={repo.id}
              className="border-0 d-flex align-items-center flex-row px-5 py-3 my-3"
            >
              <div>
                <h4>
                  <a
                    href={repo.html_url}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {repo.name}
                  </a>
                </h4>
                <p className="text-gray">{repo.description}</p>
              </div>

              <div className="ms-auto">
                <ul className="d-flex align-items-center flex-column">
                  <li className="badge badge-primary">
                    Stars: {repo.stargazers_count}
                  </li>
                  <li className="badge badge-dark">
                    Watchers: {repo.watchers_count}
                  </li>
                  <li className="badge badge-light">
                    Forks: {repo.forks_count}
                  </li>
                </ul>
              </div>
            </Card>
          </Col>
        ))
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    repos: state.profileReducers.repos,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getGithubRepos: (username) => {
      dispatch(getGithubRepos(username));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileGithub);
