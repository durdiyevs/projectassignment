import React from "react";

const ProfileTop = ({
  profile: {
    status,
    company,
    location,
    website,
    social,
    user: { name, avatar },
  },
}) => {
  return (
    <div className="text-center">
      <img className="round-img my-1" src={avatar} alt="" />
      <h1>{name}</h1>
      <p className="text-gray">
        {status} {company && <span> at {company}</span>}
      </p>
      <p className="text-dark">{location && <span>{location}</span>}</p>
      <div className="icons my-1">
        {website && (
          <a href={website} target="_blank" rel="noopener noreferrer">
            <i className="fas text-gray fa-globe me-2" />
          </a>
        )}
        {social && social.twitter && (
          <a href={social.twitter} target="_blank" rel="noopener noreferrer">
            <i className="fab text-gray fa-twitter mx-2" />
          </a>
        )}
        {social && social.facebook && (
          <a href={social.facebook} target="_blank" rel="noopener noreferrer">
            <i className="fab text-gray fa-facebook mx-2" />
          </a>
        )}
        {social && social.linkedin && (
          <a href={social.linkedin} target="_blank" rel="noopener noreferrer">
            <i className="fab text-gray fa-linkedin mx-2" />
          </a>
        )}
        {social && social.youtube && (
          <a href={social.youtube} target="_blank" rel="noopener noreferrer">
            <i className="fab text-gray fa-youtube mx-2" />
          </a>
        )}
        {social && social.instagram && (
          <a href={social.instagram} target="_blank" rel="noopener noreferrer">
            <i className="fab text-gray fa-instagram ms-2" />
          </a>
        )}
      </div>
    </div>
  );
};

export default ProfileTop;
