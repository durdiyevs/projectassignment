import React, { Fragment, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Form, InputGroup, Row, Col } from "react-bootstrap";
import { addEducation } from "../../actions/index";

const AddEducation = ({ addEducation, history }) => {
  const [formData, setFormData] = useState({
    school: "",
    degree: "",
    fieldofstudy: "",
    from: "",
    to: "",
    current: false,
    description: "",
  });

  const { school, degree, fieldofstudy, from, to, description, current } =
    formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  return (
    <Fragment>
      <h1 className="text-primary text-center">Add Your Education</h1>
      <h4 className="text-gray mb-5 text-center">
        Add any school or bootcamp that you have attended
      </h4>
      <div className="mx-auto w-50">
        <p className="text-16 text-gray">
          <span className="text-danger">*</span> = required field
        </p>
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            addEducation(formData, history);
          }}
        >
          <Row>
            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicTitle">
                <Form.Label className="text-gray">
                  School or Bootcamp<span className="text-danger">*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  name="school"
                  value={school}
                  onChange={onChange}
                  required
                />
              </Form.Group>
            </Col>

            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicCompanyName">
                <Form.Label className="text-gray">
                  Degree or Certificate
                  <span className="text-danger">*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  name="degree"
                  value={degree}
                  onChange={onChange}
                  required
                />
              </Form.Group>
            </Col>
          </Row>

          <Form.Group className="mb-3" controlId="formBasicCompanyName">
            <Form.Label className="text-gray">
              Field of Study
              <span className="text-danger">*</span>
            </Form.Label>
            <Form.Control
              type="text"
              name="fieldofstudy"
              value={fieldofstudy}
              onChange={onChange}
            />
          </Form.Group>

          <Row>
            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicFrom">
                <Form.Label className="text-gray">From</Form.Label>
                <Form.Control
                  type="date"
                  name="from"
                  value={from}
                  onChange={onChange}
                />
              </Form.Group>
            </Col>

            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicTo">
                <Form.Label className="text-gray">Untill</Form.Label>
                <Form.Control
                  type="date"
                  name="to"
                  value={to}
                  onChange={onChange}
                  disabled={current}
                />
              </Form.Group>
            </Col>
          </Row>

          <Form.Group className="mb-3" controlId="formBasicCompanyName">
            <Form.Label className="text-gray">
              Select if you are studying
            </Form.Label>
            <Form.Check
              label="Current School"
              className="text-gray p-3"
              type="checkbox"
              name="current"
              checked={current}
              value={current}
              onChange={() => {
                setFormData({ ...formData, current: !current });
              }}
            />{" "}
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicSkills">
            <Form.Label className="text-gray">Program description</Form.Label>
            <Form.Text
              className="d-block w-100"
              as="textarea"
              rows={5}
              type="text"
              name="description"
              value={description}
              onChange={onChange}
            />
          </Form.Group>

          <input type="submit" className="btn btn-primary my-1" />
          <Link className="btn btn-danger ms-2 my-1" to="/dashboard">
            Go Back
          </Link>
        </Form>
      </div>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    addEducation: (formData, history) => {
      dispatch(addEducation(formData, history));
    },
  };
};

export default connect(null, mapDispatchToProps)(withRouter(AddEducation));
