import React, { Fragment, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Col, Form, Row } from "react-bootstrap";
import { addExperience } from "../../actions/index";

const AddExperience = ({ addExperience, history }) => {
  const [formData, setFormData] = useState({
    company: "",
    title: "",
    location: "",
    from: "",
    to: "",
    current: false,
    description: "",
  });

  const [toDateDisabled, toggleDisabled] = useState(false);

  const { company, title, location, from, to, current, description } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  return (
    <Fragment>
      <h1 className="text-primary text-center">Add An Experience</h1>
      <h4 className="text-gray mb-5 text-center">
        <i className="fas fa-code-branch" /> Add any developer/programming
        positions that you have had in the past
      </h4>
      <div className="mx-auto w-50">
        <p className="text-16 text-gray">* = required field</p>
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            addExperience(formData, history);
          }}
        >
          <Row>
            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicTitle">
                <Form.Label className="text-gray">
                  Enter your title<span className="text-danger">*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  name="title"
                  value={title}
                  onChange={(e) => onChange(e)}
                  required
                />
              </Form.Group>
            </Col>

            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicCompanyName">
                <Form.Label className="text-gray">
                  Enter your Company's name
                  <span className="text-danger">*</span>
                </Form.Label>
                <Form.Control
                  type="text"
                  name="company"
                  value={company}
                  onChange={(e) => onChange(e)}
                  required
                />
              </Form.Group>
            </Col>
          </Row>

          <Form.Group className="mb-3" controlId="formBasicCompanyName">
            <Form.Label className="text-gray">
              Enter your Company's Location
              <span className="text-danger">*</span>
            </Form.Label>
            <Form.Control
              type="text"
              name="location"
              value={location}
              onChange={(e) => onChange(e)}
              required
            />
          </Form.Group>

          <Row>
            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicFrom">
                <Form.Label className="text-gray">From</Form.Label>
                <Form.Control
                  type="date"
                  name="from"
                  value={from}
                  onChange={(e) => onChange(e)}
                  required
                />
              </Form.Group>
            </Col>

            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicTo">
                <Form.Label className="text-gray">Untill</Form.Label>
                <Form.Control
                  type="date"
                  name="to"
                  value={to}
                  onChange={(e) => onChange(e)}
                />
              </Form.Group>
            </Col>
          </Row>

          <Form.Group className="mb-3" controlId="formBasicCompanyName">
            <Form.Label className="text-gray">
              Select if you are working
            </Form.Label>
            <Form.Check
              label="Current Job"
              className="text-gray"
              type="checkbox"
              name="current"
              checked={current}
              value={current}
              onChange={() => {
                setFormData({ ...formData, current: !current });
                toggleDisabled(!toDateDisabled);
              }}
            />{" "}
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicSkills">
            <Form.Label className="text-gray">
              Enter some description
            </Form.Label>
            <Form.Text
              className="d-block w-100 p-3"
              as="textarea"
              rows={5}
              type="text"
              name="description"
              value={description}
              onChange={(e) => onChange(e)}
            />
            <Form.Text className="text-muted">
              Tell us a little about your job
            </Form.Text>
          </Form.Group>

          <input type="submit" className="btn btn-primary my-1" />
          <Link className="btn btn-danger ms-2 my-1" to="/dashboard">
            Go Back
          </Link>
        </Form>
      </div>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    addExperience: (formData, history) => {
      dispatch(addExperience(formData, history));
    },
  };
};
export default connect(null, mapDispatchToProps)(withRouter(AddExperience));
