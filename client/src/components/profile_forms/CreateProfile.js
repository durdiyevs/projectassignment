import React, { Fragment, useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import { Form, Col, InputGroup, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { createProfile, getCurrentProfile } from "../../actions/index";

const initialState = {
  company: "",
  website: "",
  location: "",
  status: "",
  skills: "",
  bio: "",
  twitter: "",
  facebook: "",
  linkedin: "",
  youtube: "",
  instagram: "",
};

const CreateProfile = ({
  profileReducers: { profile, loading },
  createProfile,
  getCurrentProfile,
  history,
}) => {
  const [formData, setFormData] = useState(initialState);

  const [displaySocialInputs, toggleSocialInputs] = useState(false);

  useEffect(() => {
    if (!profile) getCurrentProfile();
    if (!loading && profile) {
      const profileData = { ...initialState };
      for (const key in profile) {
        if (key in profileData) profileData[key] = profile[key];
      }
      for (const key in profile.social) {
        if (key in profileData) profileData[key] = profile.social[key];
      }
      if (Array.isArray(profileData.skills))
        profileData.skills = profileData.skills.join(", ");
      setFormData(profileData);
    }
  }, [loading, getCurrentProfile, profile]);

  const {
    company,
    website,
    location,
    status,
    skills,
    bio,
    twitter,
    facebook,
    linkedin,
    youtube,
    instagram,
  } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    createProfile(formData, history, false);
  };

  return (
    <Fragment>
      <h1 className="text-primary text-center">Edit Your Profile</h1>
      <h4 className="text-gray mb-5 text-center">
        Add some changes to your profile
      </h4>
      <div className="mx-auto w-50">
        <p className="text-16 text-gray">
          <span className="text-danger">*</span> = required field
        </p>
        <Form className="form" onSubmit={onSubmit}>
          <Row>
            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicStatus">
                <Form.Label className="text-gray">
                  Select Professional Status
                </Form.Label>
                <Form.Select
                  name="status"
                  value={status}
                  onChange={onChange}
                  aria-label="Default select example"
                >
                  <option hidden>Professional Status</option>
                  <option value="Developer">Developer</option>
                  <option value="Junior Developer">Junior Developer</option>
                  <option value="Senior Developer">Senior Developer</option>
                  <option value="Manager">Manager</option>
                  <option value="Student or Learning">
                    Student or Learning
                  </option>
                  <option value="Instructor">Instructor or Teacher</option>
                  <option value="Intern">Intern</option>
                  <option value="Other">Other</option>
                </Form.Select>
                <Form.Text className="text-muted">
                  Give us an idea of where you are at in your career
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicCompanyName">
                <Form.Label className="text-gray">
                  Enter your compony's name
                </Form.Label>
                <Form.Control
                  type="text"
                  value={company}
                  name="company"
                  onChange={onChange}
                />
                <Form.Text className="text-muted">
                  Could be your own company or one you work for
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicWebsite">
                <Form.Label className="text-gray">
                  Enter your website
                </Form.Label>
                <Form.Control
                  type="text"
                  name="website"
                  value={website}
                  onChange={onChange}
                />
                <Form.Text className="text-muted">
                  Could be your own or a company's website
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicAddress">
                <Form.Label className="text-gray">
                  Enter your address
                </Form.Label>
                <Form.Control
                  type="text"
                  name="location"
                  value={location}
                  onChange={onChange}
                />
                <Form.Text className="text-muted">
                  City & state (eg. Boston, MA)
                </Form.Text>
              </Form.Group>
            </Col>

            <Col xs={12} md={12} lg={6}>
              <Form.Group className="mb-3" controlId="formBasicSkills">
                <Form.Label className="text-gray">Enter your skills</Form.Label>
                <Form.Control
                  type="text"
                  name="skills"
                  value={skills}
                  onChange={onChange}
                />
                <Form.Text className="text-muted">
                  Please use comma separated values (eg.
                  HTML,CSS,JavaScript,PHP)
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicSkills">
                <Form.Label className="text-gray">
                  Enter some information about you
                </Form.Label>
                <Form.Text
                  className="d-block w-100"
                  as="textarea"
                  rows={4}
                  type="text"
                  name="bio"
                  value={bio}
                  onChange={onChange}
                />
                <Form.Text className="text-muted">
                  Tell us a little about yourself
                </Form.Text>
              </Form.Group>
            </Col>
          </Row>

          <button
            onClick={() => toggleSocialInputs(!displaySocialInputs)}
            type="button"
            className="btn btn-primary d-block my-2"
          >
            Add social network links
          </button>

          {displaySocialInputs && (
            <Fragment>
              <Row>
                <Col xs={12} md={12} lg={6}>
                  <Form.Group className="mb-3 text-left">
                    <InputGroup className="mb-3">
                      <InputGroup.Text>
                        <i className="fab fa-twitter" />
                      </InputGroup.Text>
                      <Form.Control
                        type="text"
                        name="twitter"
                        value={twitter}
                        onChange={onChange}
                      />
                    </InputGroup>
                  </Form.Group>

                  <Form.Group className="mb-3 text-left">
                    <InputGroup className="mb-3">
                      <InputGroup.Text>
                        <i className="fab fa-facebook" />
                      </InputGroup.Text>
                      <Form.Control
                        type="text"
                        name="facebook"
                        value={facebook}
                        onChange={onChange}
                      />
                    </InputGroup>
                  </Form.Group>

                  <Form.Group className="mb-3 text-left">
                    <InputGroup className="mb-3">
                      <InputGroup.Text>
                        <i className="fab fa-youtube" />
                      </InputGroup.Text>
                      <Form.Control
                        type="text"
                        name="youtube"
                        value={youtube}
                        onChange={onChange}
                      />
                    </InputGroup>
                  </Form.Group>
                </Col>
                <Col xs={12} md={12} lg={6}>
                  <Form.Group className="mb-3 text-left">
                    <InputGroup className="mb-3">
                      <InputGroup.Text>
                        <i className="fab fa-linkedin" />
                      </InputGroup.Text>
                      <Form.Control
                        type="text"
                        name="linkedin"
                        value={linkedin}
                        onChange={onChange}
                      />
                    </InputGroup>
                  </Form.Group>

                  <Form.Group className="mb-3 text-left">
                    <InputGroup className="mb-3">
                      <InputGroup.Text>
                        <i className="fab fa-instagram" />
                      </InputGroup.Text>
                      <Form.Control
                        type="text"
                        name="instagram"
                        value={instagram}
                        onChange={onChange}
                      />
                    </InputGroup>
                  </Form.Group>
                </Col>
              </Row>
            </Fragment>
          )}

          <input type="submit" className="btn btn-primary my-1" />
          <Link className="btn btn-light my-1" to="/dashboard">
            Go Back
          </Link>
        </Form>
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    profileReducers: state.profileReducers,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    createProfile: (formData, history, edit) => {
      dispatch(createProfile(formData, history, edit));
    },
    getCurrentProfile: () => {
      dispatch(getCurrentProfile());
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CreateProfile));
