import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";

const ProfileItem = ({
  profile: {
    user: { _id, name, avatar },
    status,
    company,
    location,
    skills,
  },
}) => {
  return (
    <Fragment>
      <Card className="border-0 d-flex flex-row align-items-center px-5 py-4 mb-4">
        <img src={avatar} className="round-img" alt="" />

        <div>
          <h2 className="d-flex align-items-center">{name}</h2>
          <p className="text-gray">
            {status} {company && <span>at {company}</span>}
          </p>
          <p className="my-1 text-dark">
            {location && <span>{location}</span>}
          </p>
          <Link to={`/profile/${_id}`} className="btn btn-primary">
            View Profile
          </Link>
        </div>

        <ul className="ms-auto">
          {skills.slice(0, 4).map((skill, index) => (
            <li key={index} className="text-gray">
              <i className="fas fa-check"></i> {skill}
            </li>
          ))}
        </ul>
      </Card>
    </Fragment>
  );
};
export default ProfileItem;
