import React, { useEffect, Fragment } from "react";
import { connect } from "react-redux";
import { Row, Col } from "react-bootstrap";
import Spinner from "../layout/Spinner";
import ProfileItem from "./ProfileItem";
import { getAllProfiles } from "../../actions";

const Profiles = ({
  getAllProfiles,
  profileReducers: { profiles, loading },
}) => {
  useEffect(() => {
    getAllProfiles();
  }, [getAllProfiles]);
  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <h1 className="text-primary text-center">
            Developers <span className="text-dark">&</span> Engineers
          </h1>
          <h4 className="text-dark mb-0 text-center">
            Connect and collaborate with developers
          </h4>
          <div className="mt-5">
            <Row className="d-flex justify-content-center">
              <Col xs={12} md={12} lg={9}>
                {profiles.length > 0 ? (
                  profiles.map((profile, index) => (
                    <ProfileItem key={index} profile={profile} />
                  ))
                ) : (
                  <h4>No users found ...</h4>
                )}
              </Col>
            </Row>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    profileReducers: state.profileReducers,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getAllProfiles: () => {
      dispatch(getAllProfiles());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Profiles);
